<?php

namespace dsarhoya\DSYOAuth2Bundle\Entity;

use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 */
class Client extends BaseClient
{
    /**
     * @var integer
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function refreshSecret(){
        $this->setSecret(\FOS\OAuthServerBundle\Util\Random::generateToken());
    }
    
    public static function posibleGrantTypes(){
        $types = array();
        $types[] = \OAuth2\OAuth2::GRANT_TYPE_AUTH_CODE;
        $types[] = \OAuth2\OAuth2::GRANT_TYPE_CLIENT_CREDENTIALS;
        $types[] = \OAuth2\OAuth2::GRANT_TYPE_IMPLICIT;
        $types[] = \OAuth2\OAuth2::GRANT_TYPE_REFRESH_TOKEN;
        $types[] = \OAuth2\OAuth2::GRANT_TYPE_USER_CREDENTIALS;
        return $types;
    }
    
    public function allowedGrantTypesIndexes(){
        $allowed = $this->allowedGrantTypes;
        $posible = self::posibleGrantTypes();
        $indexes = array();
        foreach ($posible as $index => $grant_type) {
            if(in_array($grant_type, $allowed)){
                $indexes[] = $index;
            }
        }
        return $indexes;
    }
    
    public function canAccessResources(){
        
        return in_array(\OAuth2\OAuth2::GRANT_TYPE_IMPLICIT, $this->allowedGrantTypes);
    }
    /**
     * @var boolean
     */
    private $disabled;


    /**
     * Set disabled
     *
     * @param boolean $disabled
     * @return Client
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return boolean 
     */
    public function getDisabled()
    {
        return $this->disabled;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $accessTokens;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->accessTokens = new \Doctrine\Common\Collections\ArrayCollection();
        $this->disabled     = false;
    }

    /**
     * Add accessTokens
     *
     * @param \dsarhoya\DSYOAuth2Bundle\Entity\Accesstoken $accessTokens
     * @return Client
     */
    public function addAccessToken(\dsarhoya\DSYOAuth2Bundle\Entity\Accesstoken $accessTokens)
    {
        $this->accessTokens[] = $accessTokens;

        return $this;
    }

    /**
     * Remove accessTokens
     *
     * @param \dsarhoya\DSYOAuth2Bundle\Entity\Accesstoken $accessTokens
     */
    public function removeAccessToken(\dsarhoya\DSYOAuth2Bundle\Entity\Accesstoken $accessTokens)
    {
        $this->accessTokens->removeElement($accessTokens);
    }

    /**
     * Get accessTokens
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccessTokens()
    {
        return $this->accessTokens;
    }
}
