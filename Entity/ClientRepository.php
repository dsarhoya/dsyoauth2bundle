<?php

namespace dsarhoya\DSYOAuth2Bundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ClientRepository
 *
 * @author matias
 */
class ClientRepository extends EntityRepository{
    
    public function test(){
        return 1;
    }
    
    public function cleanClient($client){
        
        $access_token_qb = $this->createQueryBuilder('qb');
        $access_token_qb->delete('dsarhoyaDSYOAuth2Bundle:Accesstoken', 'a');
        $access_token_qb->andWhere($access_token_qb->expr()->eq('a.client', ':client'));
        $access_token_qb->setParameter(':client', $client);
        $access_token_qb->getQuery()->execute();
        
        $refresh_token_qb = $this->createQueryBuilder('qb');
        $refresh_token_qb->delete('dsarhoyaDSYOAuth2Bundle:Refreshtoken', 'r');
        $refresh_token_qb->andWhere($refresh_token_qb->expr()->eq('r.client', ':client'));
        $refresh_token_qb->setParameter(':client', $client);
        $refresh_token_qb->getQuery()->execute();
        
        
        $auth_code_qb = $this->createQueryBuilder('qb');
        $auth_code_qb->delete('dsarhoyaDSYOAuth2Bundle:Authcode', 'c');
        $auth_code_qb->andWhere($auth_code_qb->expr()->eq('c.client', ':client'));
        $auth_code_qb->setParameter(':client', $client);
        $auth_code_qb->getQuery()->execute();
        
        return true;
    }
}
