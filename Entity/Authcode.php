<?php

namespace dsarhoya\DSYOAuth2Bundle\Entity;

use FOS\OAuthServerBundle\Entity\AuthCode as BaseAuthCode;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\OAuthServerBundle\Model\ClientInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Authcode
 */
class Authcode extends BaseAuthCode
{

    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * @var \dsarhoya\DSYOAuth2Bundle\Entity\Client
     */
    protected $client;

    /**
     * @var integer
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param UserInterface $user
     * @return Authcode
     */
    public function setUser(UserInterface $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set client
     *
     * @param \dsarhoya\DSYOAuth2Bundle\Entity\Client $client
     * @return Authcode
     */
    public function setClient(ClientInterface $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \dsarhoya\DSYOAuth2Bundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }
}