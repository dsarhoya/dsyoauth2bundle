<?php

namespace dsarhoya\DSYOAuth2Bundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ClientRepository
 *
 * @author matias
 */
class AccessTokenRepository extends EntityRepository{
    
    public function findEnabledWithCriteria($criteria)
    {
        
        if(!isset($criteria['token'])) throw new \Exception('No token found');
        
        $qb = $this->createQueryBuilder('token');
        $qb->select('at');
        $qb->add('from', 'dsarhoyaDSYOAuth2Bundle:Accesstoken at');
        $qb->leftJoin('at.client', 'c');
        $qb->where($qb->expr()->andX(
                $qb->expr()->eq('at.token', ':token'),
                $qb->expr()->neq('c.disabled', $qb->expr()->literal(true))
                ));
        $qb->setParameter('token', $criteria['token']);
        
        $result = $qb->getQuery()->getResult();
        
        if($result) return $result[0];
        return null;
    }
}
