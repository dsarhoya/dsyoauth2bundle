<?php

namespace dsarhoya\DSYOAuth2Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class TestsController extends Controller
{
    public function accessTestAction() {
        
        $response = new Response();
        $response->setStatusCode(204);
        
        return $response;
    }
}
