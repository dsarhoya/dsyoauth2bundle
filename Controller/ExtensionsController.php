<?php

namespace dsarhoya\DSYOAuth2Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Process;

class ExtensionsController extends Controller
{
    public function oauthExtensionAction(Request $request)
    {
        $form = $this->createFormBuilder()
                    ->add('grant_types', 'choice', array(
                        'choices'=>  \dsarhoya\DSYOAuth2Bundle\Entity\Client::posibleGrantTypes(),
                        'multiple'=>true,
                        'expanded'=>true,
                    ))
                    ->getForm();

        if($request->getMethod()=="POST"){
            
            $form->bind($request);
            
            $posible_grant_types = \dsarhoya\DSYOAuth2Bundle\Entity\Client::posibleGrantTypes();
            $client_grant_types = array();
            foreach ($form->get('grant_types')->getData() as $type_index) {
                $client_grant_types[] = $posible_grant_types[$type_index];
            }
            
            if(count($client_grant_types) > 0){
                $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
                $client = $clientManager->createClient();
                // Esta url se usa con el grant type Auth Code (no implementado aún), por lo que da lo mismo que se pone.
                $client->setRedirectUris(array('http://www.dsarhoya.cl'));
                // Siempre hay que agregar "token" y "refresh_token". Si hay usuarios de por medio, agregar "password", si no hay usuarios (y solo oauth clients) usar "client_credentials".
                $client->setAllowedGrantTypes($client_grant_types);
                $clientManager->updateClient($client);
            }else{
                $this->get('session')->getFlashBag()->add(
                    'error',
                    'Debes seleccionar al menos un grant type'
                );
            }
            
            return $this->redirect($this->generateUrl('oauthExtension'));
        }

        return $this->render('dsarhoyaDSYOAuth2Bundle:Extensions/Oauth:OAuthExtension.html.twig', array(
            'form'=>$form->createView()
        ));
    }

    public function listOauthClientsAction(){

        $clientsRepo = $this->getDoctrine()->getRepository('dsarhoyaDSYOAuth2Bundle:Client');
        $clients = $clientsRepo->findAll();

        return $this->render('dsarhoyaDSYOAuth2Bundle:Extensions/Oauth:listOauthClients.html.twig', array(
            'clients'=>$clients
        ));
    }
    
    public function refreshSecretAction($idClient){
        
        $clientsRepo = $this->getDoctrine()->getRepository('dsarhoyaDSYOAuth2Bundle:Client');
        $client = $clientsRepo->find($idClient);
        
        if(!$client){
            
            $this->get('session')->getFlashBag()->add(
                'error',
                'Cliente no encontrado'
            );
            
            return $this->redirect($this->generateUrl('oauthClientDetails', array('idClient'=>$idClient)));
        }
        
        $client->refreshSecret();
        $clientsRepo->cleanClient($client);
        
        $this->getDoctrine()->getManager()->flush();
        
        $this->get('session')->getFlashBag()->add(
            'info',
            'Secret actualizado!'
        );
            
        return $this->redirect($this->generateUrl('oauthClientDetails', array('idClient'=>$idClient)));
    }
    
    public function deleteExpiredTokensAction($idClient){
        
        $clientsRepo = $this->getDoctrine()->getRepository('dsarhoyaDSYOAuth2Bundle:Client');
        $client = $clientsRepo->find($idClient);
        
        if(!$client){
            
            $this->get('session')->getFlashBag()->add(
                'error',
                'Cliente no encontrado'
            );
            
            return $this->redirect($this->generateUrl('oauthClientDetails', array('idClient'=>$idClient))); 
        }
        
        $process = new Process('php ../app/console fos:oauth-server:clean');
        $process->start();
        
        $this->get('session')->getFlashBag()->add(
            'info',
            'Tokens limpiados!'
        );
            
        return $this->redirect($this->generateUrl('oauthClientDetails', array('idClient'=>$idClient)));
    }
    
    public function clientDetailsAction(Request $request, $idClient){
        
        $clientsRepo = $this->getDoctrine()->getRepository('dsarhoyaDSYOAuth2Bundle:Client');
        $client = $clientsRepo->find($idClient);
        
        if(!$client){
            
            $this->get('session')->getFlashBag()->add(
                'error',
                'Cliente no encontrado'
            );
            
            return $this->redirect($this->generateUrl('oauthExtension'));
        }
        
        $options = array();
        $options['grant_types'] = $client->allowedGrantTypesIndexes();
        
        $form = $this->createFormBuilder($options)
                    ->add('grant_types', 'choice', array(
                        'choices'=>  \dsarhoya\DSYOAuth2Bundle\Entity\Client::posibleGrantTypes(),
                        'multiple'=>true,
                        'expanded'=>true,
                    ))
                    ->getForm();
        
        if($request->getMethod() == "POST"){
            
            $form->bind($request);
            
            $posible_grant_types = \dsarhoya\DSYOAuth2Bundle\Entity\Client::posibleGrantTypes();
            $client_grant_types = array();
            foreach ($form->get('grant_types')->getData() as $type_index) {
                $client_grant_types[] = $posible_grant_types[$type_index];
            }
            
            $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
            $client->setAllowedGrantTypes($client_grant_types);
            $clientManager->updateClient($client);
            return $this->redirect($this->generateUrl('oauthClientDetails', array('idClient'=>$idClient)));
        }
        
        return $this->render('dsarhoyaDSYOAuth2Bundle:Extensions/Oauth:oauthClientDetails.html.twig', array(
            'client'=>$client,
            'grant_types_form'=>$form->createView()
        ));
    }
    
    public function disableClientAction($idClient){
        
        $clientsRepo = $this->getDoctrine()->getRepository('dsarhoyaDSYOAuth2Bundle:Client');
        $client = $clientsRepo->find($idClient);
        
        $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
        $client->setDisabled(true);
        $clientManager->updateClient($client);
        
        return $this->redirect($this->generateUrl('oauthClientDetails', array('idClient'=>$idClient)));
    }
    
    public function enableClientAction($idClient){
        
        $clientsRepo = $this->getDoctrine()->getRepository('dsarhoyaDSYOAuth2Bundle:Client');
        $client = $clientsRepo->find($idClient);
        
        $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
        $client->setDisabled(false);
        $clientManager->updateClient($client);
        
        return $this->redirect($this->generateUrl('oauthClientDetails', array('idClient'=>$idClient)));
    }
}
