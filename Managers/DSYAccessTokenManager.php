<?php

namespace dsarhoya\DSYOAuth2Bundle\Managers;

use FOS\OAuthServerBundle\Entity\TokenManager;
use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;

/**
 * Description of DSYTokenManager
 *
 * @author matias
 */
class DSYAccessTokenManager extends TokenManager implements AccessTokenManagerInterface{
    
    /**
     * {@inheritdoc}
     */
    public function findTokenBy(array $criteria)
    {
        return $this->repository->findEnabledWithCriteria($criteria);
    }
}